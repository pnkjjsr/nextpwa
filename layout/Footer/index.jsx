import React, { Fragment, Component } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./style.scss";

export default class Footer extends Component {
  render() {
    return (
      <Fragment>
        <div className="footer" role="main">
          <div className="container">
            <div className="row">
              <div className="col-4 col-lg-3">
                <div className="logo">
                  <figure>
                    <img
                      className="img-fluid"
                      src="static/images/logo-t.png"
                      alt=""
                    />
                  </figure>
                </div>
              </div>
              <div className="col-lg-5  d-none d-lg-flex justify-content-end">
                <ul className="nav">
                  <li className="nav-item">
                    <a className="nav-link active" href="#">
                      Privacy Policy
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Terms & Conditions
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      About Us
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-8 col-lg-4 d-flex justify-content-end">
                <ul className="social-links">
                  <li>
                    <a className="d-flex align-items-center justify-content-center">
                      <FontAwesomeIcon icon={["fab", "facebook-f"]} />
                    </a>
                  </li>
                  <li>
                    <a className="d-flex align-items-center justify-content-center">
                      <FontAwesomeIcon icon={["fab", "twitter"]} />
                    </a>
                  </li>
                  <li>
                    <a className="d-flex align-items-center justify-content-center">
                      <FontAwesomeIcon icon={["fab", "linkedin"]} />
                    </a>
                  </li>
                  <li>
                    <a className="d-flex align-items-center justify-content-center">
                      <FontAwesomeIcon icon={["fab", "instagram"]} />
                    </a>
                  </li>
                  <li>
                    <a className="d-flex align-items-center justify-content-center">
                      <FontAwesomeIcon icon={["fab", "youtube"]} />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
