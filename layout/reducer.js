const initialState = {
    title: "Main",
    desc: "Main page description."
};

const auth = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default auth;