import React, { Fragment, Component } from "react";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";

import "./style.scss";

export default class Layout extends Component {
  render() {
    return (
      <Fragment>
        <div className="header" role="main">
          <div className="container">
            <div className="row">
              <div className="col d-flex align-items-center">
                <div className="logo">
                  <figure>
                    <img
                      className="img-fluid"
                      src="static/images/logo.gif"
                      alt=""
                    />
                  </figure>
                </div>
              </div>
              <div className="col d-none d-lg-block">
                <ul className="nav">
                  <li className="nav-item">
                    <a className="nav-link active" href="#">
                      Why <FontAwesomeIcon icon={faAngleDown} />
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      What & How <FontAwesomeIcon icon={faAngleDown} />
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      About us <FontAwesomeIcon icon={faAngleDown} />
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col d-flex justify-content-end">
                <button className="btn btn-link">Join</button>
                <button className="btn btn-outline-primary">Sign In</button>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
