import React, { Component, Fragment } from "react";
import Link from "next/link";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import homeActions from "./action";

import "./style.scss";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { home } = this.props;
    return (
      <Fragment>
        <div className="home">
          <div className="container">
            <div className="info first" role="mentor">
              <div className="row">
                <div className="col-12 col-xl-6 d-flex align-items-center justify-content-center">
                  <div className="heading">
                    Looking for
                    <br />
                    Serious Mentors
                    <small>
                      Create your mentors community to shape your future
                    </small>
                  </div>
                </div>

                <div className="col-12 col-xl-6">
                  <figure>
                    <img
                      className="img-fluid"
                      src="static/images/mentor.gif"
                      alt=""
                    />
                  </figure>
                </div>
              </div>
            </div>

            <hr />

            <div className="info" role="mentees">
              <div className="row">
                <div className="col-12 col-xl-6 d-flex flex-column justify-content-center">
                  <div className="heading">
                    Looking for
                    <br />
                    meaningful
                    <br />
                    Mentees
                    <small>
                      Create community to unleash your true potential
                    </small>
                  </div>

                  <div className="action">
                    <button className="btn btn-lg btn-primary">
                      GET STARTED
                    </button>
                  </div>
                </div>

                <div className="col-12 col-xl-6">
                  <figure>
                    <img
                      className="img-fluid"
                      src="static/images/mentees.gif"
                      alt=""
                    />
                  </figure>
                  <figcaption>
                    Give back what you have learned and see it grow unimaginable
                  </figcaption>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="home">
          <div
            className="info bg-blue d-flex flex-column align-items-center text-center"
            role="features"
          >
            <div className="container">
              <div className="heading color-black">
                What you can do on mentorIF
              </div>

              <ul className="features">
                <li>
                  <i>
                    <img src="" alt="" />
                  </i>
                  <label>CONTACT</label>
                </li>
                <li>
                  <i>
                    <img src="" alt="" />
                  </i>
                  <label>INVITE</label>
                </li>
                <li>
                  <i>
                    <img src="" alt="" />
                  </i>
                  <label>CREATE</label>
                </li>
                <li>
                  <i>
                    <img src="" alt="" />
                  </i>
                  <label>COLLABORATE</label>
                </li>
              </ul>

              <p>mentorIF is designed the way we interact in real life</p>
            </div>
          </div>

          <div
            className="info d-flex flex-column align-items-center text-center"
            role="add people"
          >
            <div className="container">
              <div className="heading color-red">
                Who are the next three people you would like to add in your real
                life...
              </div>

              <div className="row mt-5">
                <div className="col">
                  <p>Not Sure?</p>
                </div>
                <div className="col">
                  <button className="btn btn-lg btn-primary">
                    GET STARTED
                  </button>
                </div>
              </div>
            </div>
          </div>
          <hr />

          <div className="container-fluid">
            <div className="info" role="Network">
              <div className="row">
                <div className="col-12 col-xl-5">
                  <figure>
                    <img
                      className="img-fluid"
                      src="static/images/network.gif"
                      alt=""
                    />
                  </figure>
                </div>

                <div className="col-12 col-xl-7 d-flex flex-column">
                  <div className="heading color-orange">
                    Prefer quality over quantity
                  </div>
                  <p className="large">
                    Connect with people that really matter in life
                  </p>
                  <div className="action">
                    <button className="btn btn-lg btn-primary">
                      CUT NOISE
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div
            className="info bg-orange d-flex flex-column align-items-center text-center"
            role="features"
          >
            <div className="container">
              <div className="heading color-black">How mentorIF Works</div>

              <figure>
                <img className="img-fluid" src="static/images/how.gif" alt="" />
              </figure>
            </div>
          </div>

          <div className="container">
            <div
              className="info d-flex flex-column align-items-center"
              role="features"
            >
              <div className="heading color-black">What People are Saying?</div>

              <div className="row">
                <div className="col-12 col-xl-5">
                  <figure>
                    <img
                      className="img-fluid"
                      src="static/images/feedback.gif"
                      alt=""
                    />
                  </figure>
                </div>
                <div className="col-12 col-xl-7 d-flex flex-column justify-content-center">
                  <div className="heading color-orange">
                    Headline goes here Small 2 liner
                  </div>

                  <p>
                    Lorem Ipsum is simply dummy text of the printing and tyettin
                    industry. Lorem Ipsum has been the industry's industrorem
                    Ips
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  homeActions: bindActionCreators(homeActions, dispatch)
});

export default connect(
  state => state,
  mapDispatchToProps
)(Home);
